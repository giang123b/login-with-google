package com.example.loginwithgoogle

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var mGoogleSignInClient: GoogleSignInClient
    val Req_Code:Int=123
    private lateinit var firebaseAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loginWithGoogle()
    }

    private fun loginWithGoogle() {
//        FirebaseApp.initializeApp(this)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient= GoogleSignIn.getClient(this,gso)

        firebaseAuth= FirebaseAuth.getInstance()

        btnSignIn.setOnClickListener{ view: View? ->
            Toast.makeText(this,"Logging In", Toast.LENGTH_SHORT).show()
            signInGoogle()
        }
    }

    private  fun signInGoogle(){

        val signInIntent: Intent =mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent,Req_Code)

        Log.e("MainActivity", "signInGoogle")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==Req_Code){
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleResult(task)
//            firebaseAuthWithGoogle(account!!)
            Log.e("MainActivity", "onActivityResult")
        }
    }

    private fun handleResult(completedTask: Task<GoogleSignInAccount>){
        try {
            Log.e("MainActivity", "handleResult")
            val account: GoogleSignInAccount? =completedTask.getResult(ApiException::class.java)
            if (account != null) {
                UpdateUI(account)
            }
        } catch (e: ApiException){
            Toast.makeText(this,e.toString(), Toast.LENGTH_SHORT).show()
        }
    }

    private fun UpdateUI(account: GoogleSignInAccount){
        val credential= GoogleAuthProvider.getCredential(account.idToken,null)
        Log.e("MainActivity token", account.idToken.toString())
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener {task->
//            if(task.isSuccessful) {
                Log.e("MainActivity", "UpdateUI success")
                SavedPreference.setEmail(this,account.email.toString())
                SavedPreference.setUsername(this,account.displayName.toString())
                val intent = Intent(this, DetailActivity::class.java)
                startActivity(intent)
                finish()
//            }
            Log.e("MainActivity", "UpdateUI fail")
        }
    }

    override fun onStart() {
        super.onStart()
        Log.e("MainActivity", "onStart")

        if(GoogleSignIn.getLastSignedInAccount(this)!=null){
            startActivity(Intent(this, DetailActivity::class.java))
            finish()
        }
    }
}